const TARGET_X = 160;
const TARGET_Y = 460;
const TARGET_WIDTH = 300;
const TARGET_HEIGHT = 374;

const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

function getScaledHeight(sw, sh, tw) {
    const scale = tw / sw;
    return sh * scale;
}

function getScaledWidth(sw, sh, th) {
    const scale = th / sh;
    return sw * scale;
}

window.addEventListener("dragover",function(e){
  e.preventDefault();
});
window.addEventListener("drop",function(e){
  e.preventDefault();
});

function dropHandler(event) {
    event.preventDefault();
    console.log(event.dataTransfer.items[0]);
    const url = URL.createObjectURL(event.dataTransfer.items[0].getAsFile());
    const image = new Image();
    image.addEventListener('load', () => {
        const height = image.height > image.width ? getScaledHeight(image.width, image.height, TARGET_WIDTH) : TARGET_HEIGHT;
        const width = image.width >= image.height ? getScaledWidth(image.width, image.height, TARGET_HEIGHT) : TARGET_WIDTH;
        const dx = (width - TARGET_WIDTH) * 0.5;
        const dy = (height - TARGET_HEIGHT) * 0.5;

        ctx.drawImage(image, TARGET_X - dx, TARGET_Y - dy, width, height);
        ctx.drawImage(templateImage, 0, 0);
    });
    image.src = url;
}

function dragOverHandler(event) {
    // console.log(event);
}

const dropZone = document.getElementById('drop-zone');
dropZone.addEventListener('drop', dropHandler);
dropZone.addEventListener('dragover', dragOverHandler);

const templateImageUrl = new URL(
    './wolverine.png?width=587',
    import.meta.url,
);

const templateImage = new Image();
templateImage.addEventListener('load', () => {
    canvas.width = templateImage.width;
    canvas.height = templateImage.height;

    ctx.fillStyle = 'green';
    ctx.fillRect(0, 0, templateImage.width, templateImage.height);
    ctx.drawImage(templateImage, 0, 0);
});
templateImage.src = templateImageUrl;
